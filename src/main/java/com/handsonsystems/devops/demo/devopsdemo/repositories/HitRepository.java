package com.handsonsystems.devops.demo.devopsdemo.repositories;

import com.handsonsystems.devops.demo.devopsdemo.entities.Hit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HitRepository extends JpaRepository<Hit, Integer> {

}
