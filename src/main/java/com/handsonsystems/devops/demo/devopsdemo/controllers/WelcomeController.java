package com.handsonsystems.devops.demo.devopsdemo.controllers;

import com.handsonsystems.devops.demo.devopsdemo.entities.Hit;
import com.handsonsystems.devops.demo.devopsdemo.repositories.HitRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.Instant;

@Controller
public class WelcomeController {

    private final HitRepository hitRepository;

    public WelcomeController(HitRepository hitRepository) {
        this.hitRepository = hitRepository;
    }

    @GetMapping("/")
    public String welcome(Model model, HttpServletRequest request) {
        String ip = getClientIp(request);
        model.addAttribute("ip", ip);

        Hit hit = new Hit();
        hit.setVisitTime(Timestamp.from(Instant.now()));
        hit.setIpAddress(ip);
        hitRepository.save(hit);

        model.addAttribute("hits", hitRepository.count());
        return "welcome"; //view
    }

    private static String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        return remoteAddr;
    }


}
