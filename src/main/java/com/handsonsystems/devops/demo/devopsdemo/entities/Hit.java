package com.handsonsystems.devops.demo.devopsdemo.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "hits")
public class Hit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "visit_time")
    private Timestamp visitTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Timestamp getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Timestamp visitTime) {
        this.visitTime = visitTime;
    }

    @Override
    public String toString() {
        return "Hit{" +
                "id=" + id +
                ", ipAddress='" + ipAddress + '\'' +
                ", visitTime=" + visitTime +
                '}';
    }
}
