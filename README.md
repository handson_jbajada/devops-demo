# HandsOn DevOps Demo

This is a very simple application based on Spring Boot 2 using Java 8. 
It requires PostgreSQL. On startup the application automatically creates the required database schema.

The database URL is configured through environment variables or command line parameters:

- `DATASOURCE_URL` is the JDBC URL of the PostgreSQL database such as `jdbc:postgresql://localhost:5432/mydatabase`

- `DATASOURCE_USERNAME` is the database username

- `DATASOURCE_PASSWORD` is the database password 

The application can be built using Maven. It already includes the maven wrapper `mvnw` and 
the executable jar file will be built by `./mvnw clean package` 

The **executable jar file** will be generated at `/target/devops-demo-0.0.1-SNAPSHOT.jar`

When executed the application will listen on port 8080 (HTTP) and show a web page on the root URL.

## Requirements

1. Use git to retrieve the application source code and build it on the server. 
1. Install PostgreSQL using a Docker image. 
1. Deploy the application using Docker. Create a Docker file using the appropriate image to execute the application.
Configure the environment specific information in the docker file (database JDBC URL, username and password)
1. Run the application and make sure it is accessible from the public internet.

Bonus Points: Create an Ansible Playbook to deploy the docker image of the application on other hosts.



